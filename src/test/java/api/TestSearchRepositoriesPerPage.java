package api;
import automation.BaseTest;
import model.Order;
import model.Response;
import model.Sort;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;
import java.util.logging.Logger;

import static io.restassured.RestAssured.given;

@RunWith(Parameterized.class)
public class TestSearchRepositoriesPerPage extends BaseTest{
	
	 /**
     * 'egoist' user has 610 repositories published
     * The test checks Items arrangement size based on 'per_page' and 'page' values combinations
     * { test_description, user, per_page, page, expected_items_size, sort, order }
     */
    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] {
                { "Default items limit per page", "egoist", "", "",  30, Sort.DEFAULT, Order.DEFAULT },
                { "Maximum items limit per page", "egoist", "110", "",  100, Sort.DEFAULT, Order.DEFAULT },
                { "Sorting by Stars", "egoist", "", "",  30, Sort.STARS, Order.DEFAULT },
                { "Sorting By Forks", "egoist", "", "",  30, Sort.FORKS, Order.DEFAULT },
                { "Sorting By Updated", "egoist", "", "",  30, Sort.UPDATED, Order.DEFAULT },
//                ---------------- Extra scenarios ------------------------------------------------------
//                { "Custom limit per page", "egoist", "15", "",  15, Sort.DEFAULT, Order.DEFAULT },
//                { "Maximum limit providing correct maximum", "egoist", "100", "",  100, Sort.DEFAULT, Order.DEFAULT },
//                { "Default limit providing 0", "egoist", "0", "",  30, Sort.DEFAULT, Order.DEFAULT },
//                { "Default limit providing random string", "egoist", "asd", "",  30, Sort.DEFAULT, Order.DEFAULT },
//                { "Maximum limit with pre-last page", "egoist", "100", "6",  100, Sort.DEFAULT, Order.DEFAULT },
//                { "Maximum limit with last page", "egoist", "100", "7",  10, Sort.DEFAULT, Order.DEFAULT },
//                { "Custom limit with last page", "egoist", "99", "7",  16, Sort.DEFAULT, Order.DEFAULT },
//                { "Default limit with pre-last page", "egoist", "", "20",  30, Sort.DEFAULT, Order.DEFAULT },
//                { "Default limit with last page", "egoist", "", "21",  10, Sort.DEFAULT, Order.DEFAULT },
//                { "Sorting by Stars with desc order ","egoist", "", "",  30, Sort.STARS, Order.DESC },
//                { "Sorting by Forks with desc order", "egoist", "", "",  30, Sort.FORKS, Order.DESC },
//                { "Sorting by Updated with desc order", "egoist", "", "",  30, Sort.UPDATED, Order.DESC },
//                { "Sorting by Stars with asc order", "egoist", "", "",  30, Sort.STARS, Order.ASC },
//                { "Sorting by Fork with asc order", "egoist", "", "",  30, Sort.FORKS, Order.ASC },
//                { "Sorting by Updated with asc order", "egoist", "", "",  30, Sort.UPDATED, Order.ASC },
        });
    }

    private String testDescription;
    private String user;
    private String perPage;
    private String page;
    private int itemsSize;
    private Sort sort;
    private Order order;

    public TestSearchRepositoriesPerPage(String testDescription, String user, String perPage, String page, int itemsSize, Sort sort, Order order) {
        this.testDescription = String.format("---- %s ----", testDescription);
        this.user = user;
        this.perPage = perPage;
        this.page = page;
        this.itemsSize = itemsSize;
        this.sort = sort;
        this.order = order;
    }

    @Test
    public void test() {
        Logger.getAnonymousLogger().info(testDescription);
        Response response = given()
                .queryParam("q", String.format("user:%s", user))
                .queryParam("per_page", perPage)
                .queryParam("page", page)
                .queryParam("sort", sort.value)
                .queryParam("order", order.value)
                .log().uri().log().params()
                .when().get().as(Response.class);
        // read err message if response body has the key
        String msg = response.getAdditionalProperties().get("message") != null
                ? response.getAdditionalProperties().get("message").toString()
                : "";
        // assert that response body contains items if not err message is shown
        Assert.assertNotNull(msg, response.getItems());
        // assert pagination
        Assert.assertEquals(itemsSize, response.getItems().size());
        Logger.getAnonymousLogger().info("Items amount: "+ response.getItems().size());
        // assert sorting. Please see isSorted() method
        Assert.assertTrue(response.isSorted(sort, order));
    }
}


