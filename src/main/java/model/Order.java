package model;

public enum Order {
	 DEFAULT(""),
	    ASC("asc"),
	    DESC("desc");

	    public String value;

	    Order(String value) {
	        this.value = value;
	    }
	}



