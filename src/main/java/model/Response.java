package model;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import model.Item;
import model.Order;
import model.Response;
import model.Sort;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.google.common.collect.Ordering;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "total_count",
    "incomplete_results",
    "items"
})

public class Response {
	
	 @JsonProperty("total_count")
	    private Integer totalCount;
	    @JsonProperty("incomplete_results")
	    private Boolean incompleteResults;
	    @JsonProperty("items")
	    private List<Item> items = null;
	    @JsonIgnore
	    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	    @JsonProperty("total_count")
	    public Integer getTotalCount() {
	        return totalCount;
	    }

	    @JsonProperty("total_count")
	    public void setTotalCount(Integer totalCount) {
	        this.totalCount = totalCount;
	    }

	    public Response withTotalCount(Integer totalCount) {
	        this.totalCount = totalCount;
	        return this;
	    }

	    @JsonProperty("incomplete_results")
	    public Boolean getIncompleteResults() {
	        return incompleteResults;
	    }

	    @JsonProperty("incomplete_results")
	    public void setIncompleteResults(Boolean incompleteResults) {
	        this.incompleteResults = incompleteResults;
	    }

	    public Response withIncompleteResults(Boolean incompleteResults) {
	        this.incompleteResults = incompleteResults;
	        return this;
	    }

	    @JsonProperty("items")
	    public List<Item> getItems() {
	        return items;
	    }

	    @JsonProperty("items")
	    public void setItems(List<Item> items) {
	        this.items = items;
	    }

	    public Response withItems(List<Item> items) {
	        this.items = items;
	        return this;
	    }

	    @JsonAnyGetter
	    public Map<String, Object> getAdditionalProperties() {
	        return this.additionalProperties;
	    }

	    @JsonAnySetter
	    public void setAdditionalProperty(String name, Object value) {
	        this.additionalProperties.put(name, value);
	    }

	    public Response withAdditionalProperty(String name, Object value) {
	        this.additionalProperties.put(name, value);
	        return this;
	    }

	    public List<Integer> getStars() {
	        return items.stream().map(Item::getStargazersCount).collect(Collectors.toList());
	    }

	    public List<Integer> getForks() {
	        return items.stream().map(Item::getForks).collect(Collectors.toList());
	    }

	    public List<Date> getPushedAt() {
	        return items.stream().map(Item::getPushedAt).collect(Collectors.toList());
	    }

	    public Boolean isSorted(Sort sort, Order order) {
	        Map<Sort, List<? extends Comparable>> list = new HashMap<Sort, List<? extends Comparable>>() {{
	            put(Sort.DEFAULT, getStars());
	            put(Sort.STARS, getStars());
	            put(Sort.FORKS, getForks());
	            put(Sort.UPDATED, getPushedAt());
	        }};
	        Map<Order, Predicate<List<? extends Comparable>>> predicate
	                = new HashMap<Order, Predicate<List<? extends Comparable>>>() {{
	            put(Order.DEFAULT, l -> Ordering.natural().reverse().isOrdered(l));
	            put(Order.ASC, l -> Ordering.natural().isOrdered(l));
	            put(Order.DESC, l -> Ordering.natural().reverse().isOrdered(l));
	        }};
	        Logger.getAnonymousLogger().info("Sorted values:\n" + list.get(sort).toString());
	        return predicate.get(order).test(list.get(sort));
	    }

	}



