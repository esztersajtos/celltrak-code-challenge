package automation;

import utils.PropertyUtils;
import io.restassured.RestAssured;

import org.junit.BeforeClass;


public abstract class BaseTest {
	
	@BeforeClass
    public static void setup() {
        RestAssured.baseURI = PropertyUtils.getApiUri();
        RestAssured.basePath = PropertyUtils.getApiPathSearchRepositories();
    }

}
